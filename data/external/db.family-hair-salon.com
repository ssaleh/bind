$TTL 3D
@       IN      SOA     ns.family-hair-salon.com. root.family-hair-salon.com. (
                        2014110700       ; serial, todays date + todays serial #
                        8H              ; refresh, seconds
                        2H              ; retry, seconds
                        4W              ; expire, seconds
                        1D )            ; minimum, seconds
;
                NS      yosemite        ; Inet Address of name server
		NS	bugs		; NS
		NS	speedy		;
		MX	10 bugs		; Secondary 
;
localhost       A       127.0.0.1
yosemite        A       173.163.195.81
bugs		A	173.163.195.85
@		A	173.163.195.81
speedy		A	173.163.195.82
www		cname	family-hair-salon.com.
test.sam	cname	bugs
