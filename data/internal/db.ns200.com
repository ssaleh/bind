$TTL 3D
@       IN      SOA     yosemite.ns200.com. root.ns200.com. (
                        2016111200      ; serial, todays date + todays serial #
                        8H              ; refresh, seconds
                        2H              ; retry, seconds
                        4W              ; expire, seconds
                        1D )            ; minimum, seconds
;

                NS      yosemite        ; Inet Address of name server
		NS	bugs		; NS
		NS	speedy		;
		NS	homer		
		MX	10 speedy	; Secondart Mail Exchange
;
@		AAAA	2601:5c5:8200:5d00::30
		A	173.163.195.84
www		A	173.163.195.84
homer		A	54.175.58.200
yosemite	AAAA	2601:5c5:8200:5d00::30
		A	173.163.195.81
bugs		AAAA	2601:5c5:8200:5d00::31
		A	173.163.195.85
speedy		AAAA	2601:5c5:8200:5d00::32
		A	173.163.195.82
bart		A	173.163.195.84
