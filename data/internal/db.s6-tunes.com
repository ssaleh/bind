$TTL 3D
@       IN      SOA     ns.s6-tunes.com. root.s6-tunes.com. (
                        2018020800       ; serial, todays date + todays serial #
                        8H              ; refresh, seconds
                        2H              ; retry, seconds
                        4W              ; expire, seconds
                        1D )            ; minimum, seconds
;
                NS      yosemite        ; Inet Address of name server
		NS	bugs		; NS
		NS	speedy		; NS
                MX      10 yosemite 	; Primary Mail Exchanger
;
@		A	173.163.195.81
yosemite        A       173.163.195.81
yosemite        AAAA    2603:3021:1602:1c00::30
bugs		A	173.163.195.85
bugs		AAAA	2603:3021:1602:1c00::31
speedy		A	173.163.195.82
speedy		AAAA	2603:3021:1602:1c00::32
bart		A	173.195.195.84
bart		AAAA	2603:3021:1602:1c00::33
www		CNAME	s6-tunes.com.
